> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications Development

## Andrew Wong

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials 
    - Provide git command description

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create index.html
    - Create sayhello
    - Create sayhi
    - Create querybook.html

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create and forward engineer petstore ERD (Entity Relationship Diagram)
    - Provide screenshot of ERD
    - Provide screenshot of index.jsp Assignment 3 page
    - Provide links to a3.sql and a3.mwb

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Edit code in Customer.java
    - Edit code in CustomerServlet.java
    - Edit code in thanks.jsp
    - Edit code in customerform.jsp
    - Complete skill sets 10, 11, and 12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create data folder in WEB-INF/classes/crud with the files below:
    - Create ConnectionPool.java
    - Create CustomerDB.java
    - Create DBUtil.java
    - Edit code in CustomerServlet.java
    - Edit code in context.xml
    - Complete skill sets 13, 14, and 15

### Project Requirements:

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Add JQuery validation to Project 1
    - Use regexp for ensure that user enters valid input
    - Use HTML5 to limit number of characters allowed per control

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modify P2 index
    - Add CRUD functionality to web apps
    - Include server-side validation
    - Provide screenshots of P2 index in README file
