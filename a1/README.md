> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Assignment 1 Requirements:

*Three parts:*

1. Distributed version control with Git and Bitbucket
2. Java/JSP/Servlet Development installation
3. Chapter questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
* Screenshot of running http://localhost9999 (#2 above, step #4 in tutorial)
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

git init - Create an empty Git repository or reinitialize an existing one

git status -  Show the working tree status

git add - Add file contents to the index

git commit - Record changes to the repository

git push - Update remote refs along with associated objects

git pull - Fetch from and integrate with another repository or a local branch

git rm - Remove files from the working tree and from the index
 

#### Assignment Screenshots:

*Screenshot of running java Hello*:
![Running java Hello Screenshot](img/Java_Web_scrnshot.PNG)

*Screenshot of AMPPS running http://localhost*:
![Running http://localhost:9999](img/Tomcat_Screenshot.PNG)

*Screenshots of Tomcat Online portfolio*:
![Screenshot of online portfolio part 1](img/Tomcat_a1.PNG)

![Screenshot of online portfolio part 2](img/Tomcat_a1_2.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
