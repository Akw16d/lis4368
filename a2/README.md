> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Assignment 2 Requirements:

1. Create index.html
2. Create sayhello
3. Create sayhi
4. Create querybook.html

#### README.md file should include the following items:

* Screenshot of directory list before changing the name of HelloHome.html to index.html
* Screenshot of index.html
* Screenshot of sayHello
* Screenshot of sayhi
* Screenshots of querybook.html (unchecked, checked, and results)
* Screenshots of index.jsp assignment 2

#### Assignment Screenshots:

*Directory page*:
![Directory List Screenshot](img/directorypage_scrnshot.PNG)

*index.html page*:
![index.html Screenshot](img/index_html_scrnshot.PNG)

*sayhello Page*:
![sayHello Screenshot](img/sayhello_scrnshot.PNG)

*sayhi page*:
![sayhi Screenshot](img/sayhi_scrnshot.PNG)

*querybook.html Unchecked page*:
![querybook Unchecked Page Screenshot](img/query_unchecked_scrnshot.PNG)

*querybook.html Checked page*:
![querybook Checked Page Screenshot](img/query_checked_scrnshot.PNG)

*querybook.html Result page*:
![querybook.html Result Page Screenshot](img/query_scrnshot.PNG)

*index.jsp Assignement 2*:
![index.jsp Assignment 2 Page Screenshot Part 1](img/indexjsp_scrnshot1.PNG)

![index.jsp Assignment 2 Page Screenshot Part 2](img/indexjsp_scrnshot2.PNG)

![index.jsp Assignment 2 Page Screenshot Part 3](img/indexjsp_scrnshot3.PNG)

![index.jsp Assignment 2 Page Screenshot Part 4](img/indexjsp_scrnshot4.PNG)
