> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Assignment 3 Requirements:

1. Create an Entity Relationship Diagram (ERD)
2. Insert a minimum of 10 records for each table
3. Chapter questions (ch.7-8)

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of index.jsp Assignment 3 page
* Links to a3.sql and a3.mwb

#### Assignment Screenshots:

*Assignment 3 ERD*:
![A3 ERD Screenshot](img/petstore_ERD.PNG)

*Tomcat index.jsp Assignment 3 Page*:
![index.jsp Assignment 3 Screenshot](img/index_a3_scrnshot.PNG)

#### Links:

*A3.sql File*:
[A3 SQL File](docs/a3.sql)

*A3.mwb File*:
[A3 MWB File](docs/a3.mwb)
