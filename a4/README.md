> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Assignment 4 Requirements:

1. Edit code in Customer.java
2. Edit code in CustomerServlet.java
3. Edit code in thanks.jsp
4. Edit code in customerform.jsp
5. Complete skill sets 10, 11, and 12

#### README.md file should include the following items:

* Screenshot of assignment 4 main page
* Screenshot of assignment 4 failed validation
* Screenshot of assignment 4 passed validation
* Screenshots of skill sets 10, 11, and 12

#### Assignment Screenshots:

*Assignment 4 Main Page*:

![Assignment 4 Main Page Screenshot](img/a4main.PNG)

*Assingment 4 Failed Validation*:

![Assignment 4 Failed Validation Screenshot](img/a4invalid.PNG)

*Assignment 4 Passed Validation*:

![Assignment 4 Passed Validation Screenshot](img/a4valid.PNG)

*Skillset 10 - Count Characters*:

![Skillset 10 Screenshot](img/ss10.PNG)

*Skillset 11 - File Write/Read Count Words*:

![Skillset 11 Screenshot](img/ss11.PNG)

*Skillset 12 - ASCII App*:

![Skillset 12 Screenshot Part 1](img/ss12p1.PNG)

![Skillset 12 Screenshot Part 2](img/ss12p2.PNG)

![Skillset 12 Screenshot Part 3](img/ss12p3.PNG)

