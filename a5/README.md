> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Assignment 5 Requirements:

1. Create data folder in WEB-INF/classes/crud with the files below:
2. Create ConnectionPool.java
3. Create CustomerDB.java
4. Create DBUtil.java
5. Edit code in CustomerServlet.java
6. Edit code in context.xml
7. Complete skill sets 13, 14, and 15

#### README.md file should include the following items:

* Screenshot of assignment 5 valid user form entry
* Screenshot of assignment 5 passed validation
* Screenshot of assignment 5 associated database entry
* Screenshots of skill sets 13, 14, and 15

#### Assignment Screenshots:

*Assignment 5 User Form Entry*:

![Assignment 5 Valid Entry Screenshot](img/a5entry.PNG)

*Assingment 5 Passed Validation*:

![Assignment 5 Passed Validation Screenshot](img/a5passed.PNG)

*Assignment 5 Database Entry*:

![Assignment 5 Database Entry Screenshot](img/a5asso.PNG)

*Skillset 13 - Number Swap*:

![Skillset 13 Screenshot](img/wss13.PNG)

*Skillset 14 - Largest of Three Numbers*:

![Skillset 14 Screenshot](img/wss14.PNG)

*Skillset 15 - Simple Calculator Using Methods*:

![Skillset 15 Screenshot Part 1](img/wss15.PNG)

