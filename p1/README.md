> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Project 1 Requirements:

1. Add JQuery validation to Project 1
2. Use regexp for ensure that user enters valid input
3. Use HTML5 to limit number of characters allowed per control

#### README.md file should include the following items:

* Screenshot of The Online Portfolio homepage
* Screenshot of Project 1 failed validation
* Screenshot of Project 1 passed validation

#### Assignment Screenshots:

*Lis4368 Home Screen*:
![Home page Screenshot](img/OPhome.PNG)

*Project 1 Failed Validation*:
![Project 1 Failed Validation Screenshot](img/OPp1fail.PNG)

![Project 1 Failed Validation Part 2 Screenshot](img/OPp1fail2.PNG)

*Project 1 Passed Validation*:
![Project 1 Passed Validation Screenshot](img/OPp1pass.PNG)
