> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Andrew Wong

### Project 2 Requirements:

1. Modify P2 index
2. Add CRUD functionality to web apps
3. Include server-side validation
4. Provide screenshots of P2 index in README file

#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of passed validation
* Screenshot of display data
* Screenshot of modify form
* Screenshot of modified data
* Screenshot of delete warning
* Screenshots of associated database changes

#### Assignment Screenshots:

*Project 2 Valid User Form Entry*:

![Project 2 Valid Entry Screenshot](img/p2valid.PNG)

*Project 2 Passed Validation*:

![Project 2 Passed Validation Screenshot](img/p2passed.PNG)

*Project 2 Display Data*:

![Project 2 Display Data Screenshot](img/p2display.PNG)

*Project 2 Modify Form*:

![Project 2 Modify form Screenshot](img/p2update.PNG)

*Project 2 Modified Data*:

![Project 2 Modified Data Screenshot](img/p2mod.PNG)

*Project 2 Delete Warning*:

![Project 2 Delete Warning Screenshot](img/p2delete.PNG)

*Associated Database Changes (Select, Insert)*:

![Project 2 Database Screenshot Part 1](img/p2data.PNG)

*Associated Database Changes (Update)*:

![Project 2 Database Screenshot Part 2](img/p2data2.PNG)

*Associated Database Changes (Delete)*:

![Project 2 Database Screenshot Part 3](img/p2data3.PNG)
